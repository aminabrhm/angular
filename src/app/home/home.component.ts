import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'app-home',
  template: `
  <section class="hero">
    <div class="hero-body">
      <h1 class="title">
        Tasks
      </h1>
      <p class="subtitle">
        First qusetion
      </p>
      <p> Q- mention main building blocks of angular framework.</p>
      <p> A- The main building blocks is modules, components, templates, metadata, data binding, directives, services, and dependency injection</p>

      <p class="subtitle">
        Second qusetion
      </p>
      <a routerLink="/palindrome" routerLinkActive="active">palindrome</a>
      <br>
      <a routerLink="/laragest" routerLinkActive="active">laragest</a>
      <br>
      <router-outlet></router-outlet>

      <p class="subtitle">
      Third qusetion
      </p>

      <iframe width="560" height="315" src='https://dbdiagram.io/embed/611cc9502ecb310fc3d05468'> </iframe>
    </div>
  </section>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  constructor() { }
  ngOnInit(): void {
  }


}
