import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PalindromeComponent } from './palindrome/palindrome.component';
import { LaragestComponent } from './laragest/laragest.component';
import { JitCompiler } from '@angular/compiler';

const routes: Routes = [
  { path: 'palindrome', component: PalindromeComponent },
  { path: 'laragest', component: LaragestComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
