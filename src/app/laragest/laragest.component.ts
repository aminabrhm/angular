import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-laragest',
  template: `
    <p>
      Given a list of integers [1,5,2,3], Write a function that returns the largest element in this list.
    </p>
    <button (click)="laraget()">Show me the result</button>
    <div *ngIf="max">
      Laraget number is {{max}}
    </div>

  `,
  styles: [
  ]
})
export class LaragestComponent implements OnInit {

  constructor() { }
  
  max: any;
  function:any;

  ngOnInit(): void {
  }

  laraget(){
    let numbers = [1,5,2,3]
    let max= 0;
    for (let index = 0; index < numbers.length; index++) {
      if(numbers[index]>max) 
       { max = numbers[index]  }
    }
    this.max = max
  }

}
