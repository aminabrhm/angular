import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'app-palindrome',
  template: `
    <p>
      Given a string input “Dad”,”Door”,”Work”, “Nan” , Write a function that tests whether a string is a palindrome.
      <br>
      *answer will ignore capitalize otherwise all the answers will be false
    </p>
    <button (click)="palindrome('dad')">Is dad palindrom?</button>
    <button (click)="palindrome('door')">Is door palindrom?</button>
    <button (click)="palindrome('work')">Is work palindrom?</button>
    <button (click)="palindrome('nan')">Is nan palindrom?</button>
    <br>
    
    <div *ngIf="ans">
      Answer Is {{ans}}
    </div>

  `,
  styles: [
  ]
})
export class PalindromeComponent implements OnInit {

  constructor() { }
  ans:any;
  ngOnInit(): void {
  }
  palindrome(x:string){
    if(x.split('').reverse().join('') === x){
      this.ans ="True"
    } 
    else 
      this.ans ="False"
  }

}
